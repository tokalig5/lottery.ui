/**
 * Модель лотереи.
 */
export class LotteryModel {
    /**
     * Наименование лотереи.
     */
    name?: string;
}