import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '',
    children: [
      {path: 'lottery', loadChildren: () => import('./feature-modules/lottery/lottery.module').then(m => m.LotteryModule)},
      {path: 'application', loadChildren: () => import('./feature-modules/application/application.module').then(m => m.ApplicationModule)},
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
