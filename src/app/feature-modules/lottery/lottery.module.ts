import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LotteryRoutingModule } from './lottery-routing.module';
import { LotteryCreateComponent } from './components/lottery-create/lottery-create.component';
import { LotteryCardComponent } from './components/lottery-card/lottery-card.component';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { FlexLayoutModule } from "@angular/flex-layout";


@NgModule({
  declarations: [
    LotteryCreateComponent,
    LotteryCardComponent
  ],
  imports: [
    CommonModule,
    LotteryRoutingModule,
    InputTextModule,
    FormsModule, 
    ReactiveFormsModule,
    ButtonModule,
    FlexLayoutModule
  ]
})
export class LotteryModule { }
