import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { LotteryModel } from 'src/app/common/models/lottery/lottery';
import { LotteryService } from '../../services/lottery.service';

@Component({
  selector: 'app-lottery-create',
  templateUrl: './lottery-create.component.html',
  styleUrls: ['./lottery-create.component.scss']
})
export class LotteryCreateComponent implements OnInit, OnDestroy {

  destroy$ = new Subject();
  
  get lottery(): LotteryModel {
    return this.createForm.value;
  }

  createForm : FormGroup = new FormGroup({
    "name": new FormControl()
  });
  
  constructor(private _lotteryService: LotteryService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
    
  submit(): void {
    this._lotteryService.save(this.lottery).pipe(takeUntil(this.destroy$)).subscribe();
  }
}
