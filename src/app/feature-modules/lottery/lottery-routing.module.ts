import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LotteryCardComponent } from './components/lottery-card/lottery-card.component';
import { LotteryCreateComponent } from './components/lottery-create/lottery-create.component';

const routes: Routes = [
  //{path: ':id', component: LotteryCardComponent},
  {path: 'create', component: LotteryCreateComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LotteryRoutingModule { }
