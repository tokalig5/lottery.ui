import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LotteryModel } from 'src/app/common/models/lottery/lottery';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LotteryService {

  constructor(private _httpClient: HttpClient) { }

  save(lottery: LotteryModel) : Observable<any> {    
    return this._httpClient.post(`https://localhost:44302/api/v1/lot`, lottery);
  }
}
