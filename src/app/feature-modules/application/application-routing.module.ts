import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationCardComponent } from './components/application-card/application-card.component';
import { ApplicationCreateComponent } from './components/application-create/application-create.component';

const routes: Routes = [
  {path: ':id', component: ApplicationCardComponent},
  {path: 'create', component: ApplicationCreateComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
