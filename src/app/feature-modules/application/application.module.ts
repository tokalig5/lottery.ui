import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationCreateComponent } from './components/application-create/application-create.component';
import { ApplicationCardComponent } from './components/application-card/application-card.component';


@NgModule({
  declarations: [
    ApplicationCreateComponent,
    ApplicationCardComponent
  ],
  imports: [
    CommonModule,
    ApplicationRoutingModule
  ]
})
export class ApplicationModule { }
